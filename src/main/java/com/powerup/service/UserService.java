package com.powerup.service;

import com.powerup.dto.LoginRequest;
import com.powerup.dto.ResponseBody;
import com.powerup.dto.UserDto;
import com.powerup.interfaces.IUserService;
import com.powerup.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService implements IUserService {

    private final UserRepository userRepository;

    @Override
    public ResponseBody signUp(UserDto newUser) {
        return null;
    }

    @Override
    public ResponseBody login(LoginRequest loginObject) {
        return null;
    }
}
