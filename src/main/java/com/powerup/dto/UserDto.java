package com.powerup.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class UserDto {

    @NotBlank(message = "Name is required")
    private String name;

    @Schema(example = "test@mail.com")
    @NotBlank(message = "Email is required")
    @Pattern(regexp = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", message = "Invalid email format")
    private String email;

    @NotBlank(message = "FamilyName is required")
    private String familyName;

    @NotBlank(message = "Address is required")
    private String address;

    @NotBlank(message = "PhoneNumber is required")
    private String phoneNumber;

    @Schema(example = "Pa55word*")
    @NotBlank(message = "Password is required")
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[*_-]).{8,15}$",
            message = "Password has to be between 8 and 15 characters and must have uppercase, lowercase, numbers and at least one character like '*_-'")
    private String password;
}
