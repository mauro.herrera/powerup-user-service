package com.powerup.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ToString
public class LoginRequest {

    @NotBlank(message = "User required")
    private String user;

    @NotBlank(message = "Password required")
    private String password;
}
