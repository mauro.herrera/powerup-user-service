package com.powerup.interfaces;

import com.powerup.dto.LoginRequest;
import com.powerup.dto.ResponseBody;
import com.powerup.dto.UserDto;

public interface IUserService {

    ResponseBody signUp(UserDto newUser);
    ResponseBody login(LoginRequest loginObject);
}
