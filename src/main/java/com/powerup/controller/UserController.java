package com.powerup.controller;

import com.powerup.dto.LoginRequest;
import com.powerup.dto.ResponseBody;
import com.powerup.dto.UserDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/user")
public class UserController {


    @PostMapping(path = "/signup")
    ResponseEntity<ResponseBody> createUser(@RequestBody @Valid UserDto newUser) {
        return new ResponseEntity<>(new ResponseBody("User creation succeed", "OK", "200"), HttpStatus.OK);
    }

    @PostMapping(path = "/login")
    ResponseEntity<ResponseBody> loginUser(@RequestBody @Valid LoginRequest newUser) {
        return new ResponseEntity<>(new ResponseBody("Login succeed", null, "200"), HttpStatus.OK);
    }
}
